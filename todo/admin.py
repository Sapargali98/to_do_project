from .models import Task
from django.contrib import admin

# Register your models here.
class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'status', 'created_at']
    list_filter = ['status']
    search_fields = ['title']
    fields = ['title', 'description', 'status', 'created_at']
    readonly_fields = ['created_at']

admin.site.register(Task, TaskAdmin)
