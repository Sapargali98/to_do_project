from django.db import models
from django.contrib.auth import get_user_model

class Tag(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name
class Task(models.Model):
    STATUS_CHOICES = (
        ('in progress', 'In Progress'),
        ('completed', 'Completed'),
        ('pending', 'Pending'),
    )
    title = models.CharField(max_length=200)
    description = models.TextField()
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ManyToManyField(get_user_model(), related_name='tasks', verbose_name='Author')
    class Meta:
        permissions = [
            ('special_status', 'Can change the special status'), 
        ]
    def __str__(self):
        return self.title