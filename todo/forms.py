from django import forms
from .models import Task
from django import forms
from django.db.models import Q

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'user': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Author'})
        }

class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')

class TaskFilterForm(forms.Form):
    status = forms.ChoiceField(choices=[('', 'All'), ('in progress', 'In Progress'), ('completed', 'Completed'), ('pending', 'Pending')], required=False)
    created_at = forms.DateField(input_formats=['%b %d %Y'], required=False, error_messages={'invalid': 'Enter a date like this: Mon DD YYYY'})
    search = forms.CharField(max_length=100, required=False)

    def filter_queryset(self, queryset):
        query = Q()
        if self.is_valid():
            status = self.cleaned_data['status']
            if status:
                query &= Q(status=status)
            created_at = self.cleaned_data['created_at']
            if created_at:
                query &= Q(created_at__date=created_at)
            search = self.cleaned_data['search']
            if search:
                query &= Q(title__icontains=search) | Q(description__icontains=search)
        return queryset.filter(query)
