from urllib import request
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from todo.forms import TaskFilterForm, TaskForm,SearchForm
from todo.models import Task 
from django.views.generic import ListView, DetailView, CreateView, DeleteView,UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
def home(request):
    return render(request, 'home.html')
class TaskCreateView(LoginRequiredMixin,CreateView):
    model = Task
    form_class = TaskForm
    template_name = 'task_form.html'
    success_url = reverse_lazy('task-list')
def form_valid(self, form):
    form.instance.author = self.request.user
class TaskListView(LoginRequiredMixin,ListView):
    model= Task
    template_name = 'tasklist.html'
    form = SearchForm
    paginate_by = 5
    paginate_orphans = 1
    search_value = None 

    def get_queryset(self):
        queryset = super().get_queryset().filter(author=self.request.user)
        form = TaskFilterForm(self.request.GET)
        if form.is_valid():
            queryset = form.filter_queryset(queryset)
        return queryset
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TaskFilterForm(self.request.GET)
        return context
class TaskUpdateView(LoginRequiredMixin,UpdateView):
    model = Task
    form_class = TaskForm
    template_name = 'task_form.html'
    def get_queryset(self):
        qs = super().get_queryset().filter(author=self.request.user)
        if not qs.exists():
            raise PermissionDenied
        return qs
    def get_success_url(self):
        return reverse('task-detail', kwargs={'pk': self.object.pk})
class TaskDeleteView(LoginRequiredMixin,DeleteView):
    model = Task
    template_name = 'task_delete.html'
    def get_queryset(self):
        return super().get_queryset().filter(author=self.request.user)

    def get_success_url(self):
        return reverse_lazy('task-list')
class TaskDetailView(DetailView):
    model = Task
    template_name = 'task_detail.html'
    context_object_name = 'task'
    
    def get_queryset(self):
        return super().get_queryset().filter(author=self.request.user)

    def get_success_url(self):
        return reverse('task-detail', kwargs={'pk': self.object.pk})
